// Copyright 2021-2022, University of Colorado Boulder

/**
 * Auto-generated from modulify, DO NOT manually modify.
 */
/* eslint-disable */
import getStringModule from '../../chipper/js/getStringModule.js';
import geometricOptics from './geometricOptics.js';

type StringsType = {
  'geometric-optics': {
    'title': string
  },
  'screen': {
    'lens': string,
    'mirror': string
  },
  'convexLens': string,
  'concaveLens': string,
  'convexMirror': string,
  'concaveMirror': string,
  'flatMirror': string,
  'objectN': string,
  'focalPoint': string,
  'focalPoints': string,
  'twoF': string,
  'twoFPoints': string,
  'labels': string,
  'radiusOfCurvaturePositive': string,
  'radiusOfCurvatureNegative': string,
  'indexOfRefraction': string,
  'focalLengthPositive': string,
  'focalLengthNegative': string,
  'diameter': string,
  'guides': string,
  'realImageN': string,
  'virtualImage': string,
  'virtualImageN': string,
  'none': string,
  'marginal': string,
  'principal': string,
  'many': string,
  'secondPoint': string,
  'valueCentimetersPattern': string,
  'arrow': string,
  'pencil': string,
  'penguin': string,
  'light': string,
  'centimeters': string,
  'rays': string,
  'projectionScreen': string,
  'opticalAxis': string,
  'focalLengthControl': string,
  'direct': string,
  'indirect': string,
  'keyboardHelpDialog': {
    'moveDraggableItems': string,
    'move': string,
    'moveSlower': string,
    'rulerControls': string,
    'removeFromToolbox': string,
    'returnToToolbox': string,
    'jumpRuler': string,
    'chooseAnObject': string,
    'objects': string,
    'object': string
  }
};

const geometricOpticsStrings = getStringModule( 'GEOMETRIC_OPTICS' ) as StringsType;

geometricOptics.register( 'geometricOpticsStrings', geometricOpticsStrings );

export default geometricOpticsStrings;
