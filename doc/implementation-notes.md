# Geometric Options - Implementation Notes

@author Martin Veillette, Chris Malley (PixelZoom, Inc.)

Ths document is meant and to supplement the source code and comments of the simulation Geometric Optics.

## Terminology 

Since the language of optics is confusing, and terms overlap with those used in software development, it is
worthwhile to define some terms uses throughout the simulation.

**Object:**  
Anything that can be viewed. Unfortunately this term conflicts with JavaScript's `Object` type, so we 
use **FramedObject** in the code.

**Image:**
The likeness of an object produced at a point in space by a lens or a mirror.
Unfortunately this term conflicts with `SCENERY/Image` so we use the term **FramedImage** in the code.

**Real image:**
An image for which light rays physically intersect at the image location.

**Virtual Image:**
A image for which light rays do not physically intersect at the image point but appears to diverge from that point.

**Real Rays:**
Light rays emanating from an object that are reflected/transmitted by an optical element

**Virtual Rays:**
Backward rays that indicate an apparent origin for the divergence of rays. Virtual rays are drawn from an optical
element to the position of a virtual image.

**Optical Axis:**
The straight line passing through the center of curvature and pole of  
an optical element. It is also called the "principal axis".

**First Principal Focus:**
A beam of light incident parallel to the optical axis, after reaching the optical element, will either actually converge
to or appear to diverge from a fixed point on the optical axis. The fixed point is called the "First Principal focus".

**Second Principal Focus:**
The point opposite to the first principal focus from the optical element.

**Guide:**
This is a PhET construction, but is used in the simulation to denote the bending of the light due to a lens. A guide
is attached to the ends of the lens and can freely rotate from its fulcrum point.

**Screen:**
Light is projected onto a screen. Unfortunately this term conflicts with `SCENERY/Screen`. So we use _ProjectorScreen_
throughout the code.

**Experiment Area:** This simulation creates a scenery layer called **experiment area** that includes all UI elements
that are affected by the sim's zoom buttons. It is important to note that the rulers and the labels do not belong to the
experiment area, since they contain text that may be hard to read upon zooming.

**positive/negative**: The meaning of positive and negative is a convention that may vary. We define the convention
followed in the simulation in [model.md](https://github.com/phetsims/geometric-optics/blob/master/doc/model.md).

**distance**: In optics, a distance is always measured horizontally, and can be positive or negative. 

**height**: The height in optics is always measured from the optical axis. A positive (negative) height indicates the object is above (below) the optical axis.

## General Considerations

### Model-view transform and Zoom

This simulation makes use of model-view transform to map model coordinates to the view coordinates. The base units of
the model is centimeters (cm). It is used throughout the model with a few exceptions that have been noted. A model-view
transform is applied to all elements within the experiment area. All elements within the experiment area can be scaled
up and down by scaling `experimentAreaNode`. The origin (0,0) in the model coordinate frame is near the center of the
ScreenView. The model-to-view scaling is isometric along the horizontal and vertical directions.

For scenery Nodes outside the experimentAreaNode, we lay them out using view coordinates. There are two exceptions to
this:
(1) The `FramedObjectSceneLabelsNode`, responsible for labels beneath the optical components and (2) the `GORulerNode`. The
labels and ruler use `zoomTransformProperty` which allows it to relate its coordinates to within the experiment area at
a particular zoom level.

The Nodes within the experiment area may need to know about the position of objects outside the experiment area, such as
the bounds of the simulation. For instance, the `zoomTransform` can be used to convert the visibleBounds of the
simulation to `modelBounds`.

### Memory management

* **Dynamic allocation:** Most objects in this sim are allocated at startup, and exist for the lifetime of the simulation. 
The exception is `GORulerNode`, which is instantiated each time the zoom level changes. 

* **Listeners**: Unless otherwise noted in the code, all uses of `link`, `addListener`, etc. do NOT need a corresponding
  `unlink`, `removeListener`, etc.

* **dispose**: Most sim-specific classes are not intended to be disposed, and therefore do not properly implement
`dispose`.  Those classes will either have no `dispose` method, or will override their interited `dispose`
method like this:

```js
/**
 * @public
 * @override
 */
dispose() {
  assert && assert( false, 'dispose is not supported, exists for the lifetime of the sim' );
  super.dispose();
}
```

# Model

The main model class
is [GOModel](https://github.com/phetsims/geometric-optics/blob/master/js/common/model/GeometricOpticsModel.js)
.

There are a three top-level model elements in GOModel that play an essential role, namely `FramedObject`
, `Optic` and `FramedImage`. This trifecta of elements rules the entire simulation. Each of them is a component of the
thin-lens and mirror equation. It is important to note that all the light rays do not drive the model, but take their
marching orders from the trifecta.

* [FramedObject](https://github.com/phetsims/geometric-optics/blob/master/js/common/model/SourceObject.js) is the first
  object/source.
* [SecondPoint](https://github.com/phetsims/geometric-optics/blob/master/js/common/model/SecondSource.js) is the second
  object/source.
* [Optic](https://github.com/phetsims/geometric-optics/blob/master/js/common/model/Optic.js) is the optic, a lens or
  mirror. It is responsible for the optical element position, diameter, curvature radius and refractive index. The
  previous Properties are used to determine the focal length. Optic is also responsible for the shape of the optical
  element, which can be used for ray hit-testing, as well as drawing its shape.
* [FramedImage](https://github.com/phetsims/geometric-optics/blob/master/js/common/model/Target.js) is the optical image (real or vitual). It is responsible for the
  position of the target, its bounds and scale. It includes multiple methods that determine if the target is
  real/virtual, inverted/upright, etc.

The client can select from the combox box what we refer to as a representation of the object/source.

* [Representation](https://github.com/phetsims/geometric-optics/blob/master/js/common/model/Representation.js) is the
  class responsible for the representation of the object and target. The representation provide a map to images with
  different orientations that can be used to display source/object images and target images as well as the image logo.
  The position of "interesting" points within images is defined in the representation.

Light rays form an important aspect of this simulation. There are three model classes responsible for the rays:

* [Ray](https://github.com/phetsims/geometric-optics/blob/master/js/common/model/Ray.js) is a representation of a
  finite, or semi-infinite straight rays. A ray can only be straight, not refracted. Note that `Ray` extends `dot/Ray2`.
* [LightRay](https://github.com/phetsims/geometric-optics/blob/master/js/common/model/LightRay.js) is a representation
  of a ray emanating from a source or object. `LightRay` has a time dependencies. A `LightRay` can be refracted or
  reflected from an optical element. It can even fork into a virtual and real ray. A `LightRay` is usually composed of one or more
  `Rays`. A `LightRay` converts its rays at that point in time into `kite/Shape`. It can indicate if it has reached a target
  or the projector screen.
* [LightRays](https://github.com/phetsims/geometric-optics/blob/master/js/common/model/LightRays.js) is a representation
  of a bundle of light rays. LightRays depends on the `RayMode`. The bundle of light rays emerge from a single
  object/source position. An additional responsibility of LightRays is to indicate if one of its ray has reached a
  target, or projector screen.

We note that each light ray depends on the trifecta (FramedObject, Optic and FramedImage) and their path is determined based
on this information. This insures that all rays can converge to the same target.

# View

There are a few top-level view elements:

* [OpticNode](https://github.com/phetsims/geometric-optics/blob/master/js/common/view/OpticNode.js) renders
  the optical element (Lens or Mirror).
* [FramedObjectNode](https://github.com/phetsims/geometric-optics/blob/master/js/common/view/SourceObjectNode.js) renders framed objects.
* [SecondPointNode](https://github.com/phetsims/geometric-optics/blob/master/js/common/view/SecondSourceNode.js) renders the second point-of-interest on a framed object.
* [FramedImageNode](https://github.com/phetsims/geometric-optics/blob/master/js/common/view/FramedImageNode.js) renders
  the optical image associated with a framed object.
* [RealLightRaysNode](https://github.com/phetsims/geometric-optics/blob/master/js/common/view/RealLightRaysNode.js) and 
[VirtualLightRaysNode](https://github.com/phetsims/geometric-optics/blob/master/js/common/view/VirtualLightRaysNode.js)
  render real and virtual light rays respectively.

Properties in [VisibileProperties](https://github.com/phetsims/geometric-optics/blob/master/js/common/view/VisibleProperties.js) are used to toggle the visibility of Nodes.

## Gotchas

There a few odd things.

* Since LightRays have a dependency on the `projectorScreen`, the `projectorScreen` model is instantiated within the
  common model. However, we note that there is no counterpart `ProjectionScreenNode` within the `MirrorScreen` since the
  mirror screen does not have a light source representation.
* The `Optic` model takes an index of refraction has a parameter. Physical mirror do not have an index of refraction,
  but for the purposes of the simulation, we can make our model mirror to be functionally equivalent to a lens with an
  index of refraction of 2.
* The shape of the lens as well as the refraction of the rays within the lens is hollywooded. This leads to a few
  artefacts that we attempted to minimize, but unfortunately complicates the codebase.
* There is a not a one-to-one correspondence between the model instances and view instances.
    - There is one instance of opticNode that depends on one optic model (so far so good).
    - There is one instance of targetNode that depends on the firstTarget model. The secondTarget model does not have a
      targetNode component but is used by the secondLightRay and projectorScreen.
    - There is one objectSourceNode and one objectSource model. The position of the first and second source are embedded
      within the `objectSourceModel`.
